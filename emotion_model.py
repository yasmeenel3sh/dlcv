import keras
import numpy as np
import glob
from PIL import Image
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

"""
Team Name:	Will Be Done

Yasmeen Khaled
37-6614
yasmeen.abdelmohsen@student.guc.edu.eg
yasmeenel3sh@gmail.com

Michael Khalil
37-3063
michael.khalil@student.guc.edu.eg
mikegeorge2012@gmail.com

Olfat Mostafa
37-19029
olfat.aaf@student.guc.edu.eg
olfatm17@gmail.com
"""

#3 classes (angry,happy,sad) with grayscale images of size 48x48 and batch size of 32
num_classes = 3
rows, cols = 48, 48
batch_size = 32

train_angry=3995
train_happy=7215
train_sad=4830
train_data_size=train_angry+train_happy+train_sad
val_angry=491
val_happy=879
val_sad=594
val_data_size=val_angry+val_happy+val_sad

#augmenting the training data to create variations of the training images for more regularization of the data set and normalizing the images.
tDatagen = ImageDataGenerator(
					rescale=1./255,
					rotation_range=30,
					shear_range=0.3,
					zoom_range=0.3,
					horizontal_flip=True,
					fill_mode='nearest')

#the train directory contains 3 folders for each type of emotion so the batches of size 32 are created with class_indices {'Angry': 0, 'Happy': 1, 'Sad': 2} .
train_generator = tDatagen.flow_from_directory(
					'train/',
					color_mode='grayscale',
					target_size=(rows,cols),
					batch_size=batch_size,
					class_mode='categorical',
					shuffle=True)

vDatagen = ImageDataGenerator(rescale=1./255)

validation_generator = vDatagen.flow_from_directory(
							'validate/',
							color_mode='grayscale',
							target_size=(rows,cols),
							batch_size=batch_size,
							class_mode='categorical',
							shuffle=True)


# {'Angry': 0, 'Happy': 1, 'Sad': 2}


#sequential model of modified VGG architecture 
model = Sequential()
#First Block of 2 conv layers with 64 3x3 filters, followed by batch normaliztion, maxpooling reducing the size by half and dropout by 20%
model.add(Conv2D(input_shape=(rows,cols,1),filters=64,kernel_size=(3,3),padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=64,kernel_size=(3,3),padding="same", kernel_initializer='he_normal',activation="elu"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
model.add(Dropout(0.2))

#Second Block of 2 conv layers with 128 3x3 filters, followed by batch normaliztion, maxpooling reducing siz2 by half and dropout by 20%
model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
model.add(Dropout(0.2))

#Third Block of 3 conv layers with 256 3x3 filters, followed by batch normaliztion, maxpooling reducing siz2 by half and dropout by 20%
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
model.add(Dropout(0.2))

#Fourth Block of 3 conv layers with 512 3x3 filters, followed by batch normaliztion, maxpooling reducing siz2 by half and dropout by 20%
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
model.add(Dropout(0.2))

#Fifth Block of 3 conv layers with 256 3x3 filters, followed by batch normaliztion, maxpooling reducing siz2 by half and dropout by 20%
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same",kernel_initializer='he_normal', activation="elu"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
model.add(Dropout(0.2))

#Fully Connected Layers
#Flattening followed by a Dense layer of size 64, batch normalization and dropout of 50%
model.add(Flatten())
model.add(Dense(units=64,kernel_initializer='he_normal',activation="elu"))
model.add(BatchNormalization())
model.add(Dropout(0.5))

#Dense layer of size 64, batch normalization and dropout of 50%
model.add(Dense(units=64,kernel_initializer='he_normal',activation="elu"))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation="softmax"))
print(model.summary())


#saving the model with checkpoints depending on minimum val_loss
checkpoint = ModelCheckpoint('Emotion_elu_50_64.h5',
                             monitor='val_loss',
                             mode='min',
                             save_best_only=True,
                             verbose=1)

#early stopping callback with patience 9, ie. if 9 epochs went on with no reduction on the val_loss stop the training
earlystop = EarlyStopping(monitor='val_loss',
                          min_delta=0,
                          patience=9,
                          verbose=1,
                          restore_best_weights=True
                          )

#reduce the learning rate on 3 consectives epochs with no reduction of val_loss by 20%
reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                              factor=0.2,
                              patience=3,
                              verbose=1,
                              min_delta=0.0001)

callbacks = [earlystop,checkpoint,reduce_lr]

#compiling the model using crossentropy to calculate the loss and adam optimizer with initial learning rate of 0.001
model.compile(loss='categorical_crossentropy',
              optimizer = Adam(lr=0.001),
              metrics=['accuracy'])

epochs=50

#training the model using the batchs created from the train and validate generators with list of callbacks defined above and steps = to number of images/batch size
h=model.fit_generator(
                train_generator,
                steps_per_epoch=train_data_size//batch_size,
                epochs=epochs,
                callbacks=callbacks,
                validation_data=validation_generator,
                validation_steps=val_data_size//batch_size)



# test_set = []
# for i in range(30):
#     if(i<10):
#         test = np.array(Image.open('test/angry/' + str(i) + '.jpg'))
#     elif(i<20):
#         test = np.array(Image.open('test/happy/' + str(i-10) + '.jpg'))
#     else:
#         test = np.array(Image.open('test/sadness/' + str(i-20) + '.jpg'))    
#     test_set.append(test)
# test_set = np.array(test_set)

# m = model.predict(test_set.reshape(test_set.shape[0],48,48,1))
# print(m)
# test_accur=0
# for i in range(30):
#     maxval=np.argmax(m[i])
#     if(maxval==0):
#         print("angry")
#         if(i<10):
#             test_accur+=1
#     elif(maxval==2):
#         print("happy")
#         if(10<=i<20):
#             test_accur+=1
#     else:
#         print("saddness")
#         if(20<=i<30):
#             test_accur+=1

# test_accur=(test_accur/30)*100           
# print(test_accur)
    

