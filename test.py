from keras.models import load_model
from time import sleep
from keras.preprocessing.image import img_to_array
from keras.preprocessing import image
import numpy as np
import glob
from PIL import Image

classifier =load_model('Emotion_high.h5')
class_labels = [0.0,1.0,2.0]
rows=48
cols=48
test_data=[]
test_data_names=[]
labels=[]
for i, emotion in enumerate(['angry','happy', 'sad']):
    filelist = glob.glob('made/'+emotion + '/*.png')
    x = np.array([np.array(Image.open(fname)) for fname in filelist])
    if(len(test_data) > 0):
        test_data = np.append(test_data, x, axis=0)
        test_data_names = np.append(test_data_names, filelist)
    else:
        test_data = x
        test_data_names = filelist
    labels = np.append(labels, np.ones(x.shape[0]) * i)
test_data = test_data.reshape(test_data.shape[0],rows,cols,1)
test_data=np.multiply(test_data,1.0/255)
acc=0    
preds = classifier.predict(test_data)
for img in range(0,len(test_data)):
  label=  class_labels[preds[img].argmax()]
  print('Was ' + str(labels[img]) + ', predicted ' + str(label) + ' for' + test_data_names[img])
  if(label==labels[img]):
    acc+=1


acc=acc/len(labels)    
print(acc) 